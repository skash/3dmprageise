# 3dMPRAGEise

Quick MP2RAGE UNI background cleaning script. 

All **AFNI**. No other dependencies.

Usage: `3dMPRAGEise INV2.nii UNI.nii`

# Example
Comparing to [MP2RAGE Robust Script](https://github.com/JosePMarques/MP2RAGE-related-scripts/blob/master/DemoRemoveBackgroundNoise.m)

![Coronal](img/coronal.png)


![Axial](img/axial.png)



